import React, { useEffect, useState } from 'react';

function PresentationForm(props) {
    const [presenterName, setPresName] = useState('');
    const [presenterEmail, setEmail] = useState('');
    const [theconference, setConference] = useState('');
    const [conferences, setConferences] = useState([]);
    const [companyName, setCompany] = useState('');
    const [TitleName, setTitle] = useState('');
    const [SynoposisInfo, setSynopsis] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.presenter_name = presenterName;
        data.presenter_email = presenterEmail;
        data.conference = theconference;
        data.company_name = companyName;
        data.title = TitleName;
        data.synopsis = SynoposisInfo;

        const presentationURL = "http://localhost:8000" + theconference + "presentations/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
              },
        };

        const response = await fetch(presentationURL, fetchConfig)
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);
            setPresName('');
            setEmail('');
            setConference('');
            setCompany('');
            setTitle('');
            setSynopsis('');
        }

    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setConferences(data.conferences);
        }
      }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setPresName(value);
    }

    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value);
    }

    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }

    const handleCompanyChange = (event) => {
        const value = event.target.value;
        setCompany(value);
    }

    const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value);
    }

    const handleSynopsisChange = (event) => {
        const value = event.target.value;
        setSynopsis(value);
    }

      useEffect(() => {
        fetchData();
      }, []);





return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input placeholder="Presenter name" onChange={handleNameChange} value={presenterName} required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Presenter email" onChange={handleEmailChange} value={presenterEmail} required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Company name" onChange={handleCompanyChange} value={companyName} type="text" name="company_name" id="company_name" className="form-control"/>
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Title" onChange={handleTitleChange} value={TitleName} required type="text" name="title" id="title" className="form-control"/>
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea className="form-control" onChange={handleSynopsisChange} value={SynoposisInfo} id="synopsis" rows="3" name="synopsis" ></textarea>
              </div>
              <div className="mb-3">
                <select required name="conference" onChange={handleConferenceChange} value={theconference} id="conference" className="form-select">
                  <option value="">Choose a conference</option>
                  {conferences.map(conference => {
                    return (
                        <option value={conference.href} key={conference.href}>
                            {conference.name}

                        </option>
                    );
                 })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
        </div>

)
}
export default PresentationForm;
