import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';


function ConferenceForm (props) {
  const [name, setName] = useState('');
  const [starts, setStart] = useState('');
  const [ends, setEnd] = useState('');
  const [description, setDescription] = useState('');
  const [maxpresentations, setMaxPres] = useState('');
  const [maxattendees, setMaxAtt] = useState('');
  const [thelocation, setLocation] = useState('');
  const [locations, setLocations] = useState([]);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {}
    data.name = name;
    data.starts = starts;
    data.ends = ends;
    data.description = description;
    data.max_presentations = maxpresentations;
    data.max_attendees = maxattendees;
    data.location = thelocation;

    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);
      setName('');
      setStart('');
      setEnd('');
      setDescription('');
      setMaxPres('');
      setMaxAtt('');
      setLocation('');
    }
  }

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);

}

const handleStartsChange = (event) => {
  const value = event.target.value;
  setStart(value);

}

const handleEndsChange = (event) => {
  const value = event.target.value;
  setEnd(value);

}

const HandleDescriptionChange = (event) => {
  const value = event.target.value;
  setDescription(value);

}

const HandleMaxPresChange = (event) => {
  const value = event.target.value;
  setMaxPres(value);

}

const HandleMaxAttChange = (event) => {
  const value = event.target.value;
  setMaxAtt(value);

}

const handleLocationChange = (event) => {
  const value = event.target.value;
  setLocation(value);

}


  useEffect(() => {
    fetchData();
  }, []);

    return (
      <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input placeholder="Name" onChange={handleNameChange} value={name} required type="text" name="name" id="name" className="form-control"/>
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input placeholder="mm/dd/yyy" onChange={handleStartsChange} value={starts} required type="date" name="starts" id="starts" className="form-control"/>
              <label htmlFor="starts">Starts</label>
            </div>
            <div className="form-floating mb-3">
              <input placeholder="mm/dd/yyy" onChange={handleEndsChange} value={ends} required type="date" name="ends" id="ends" className="form-control"/>
              <label htmlFor="ends">Ends</label>
            </div>
            <div className="mb-3">
              <label htmlFor="description" className="form-label">Description</label>
              <textarea className="form-control" onChange={HandleDescriptionChange} value={description} name="description" id="description" rows="3"></textarea>
            </div>
            <div className="form-floating mb-3">
              <input placeholder="Maximum Presentations" onChange={HandleMaxPresChange} value={maxpresentations} required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
              <label htmlFor="max_presentations">Maximum presentations</label>
            </div>
            <div className="form-floating mb-3">
              <input placeholder="Maximum attendees" onChange={HandleMaxAttChange} value={maxattendees} required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
              <label htmlFor="max_presentations">Maximum attendees</label>
            </div>
            <div className="mb-3">
              <select onChange={handleLocationChange} value={thelocation} required name="location" id="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                    return (
                        <option value={location.id} key={location.id}>
                            {location.name}
                        </option>

                    );
                  })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
    );
}

export default ConferenceForm;
