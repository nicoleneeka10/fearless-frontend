function createCard(name, description, pictureUrl, startdateformatted, enddateformatted, location) {
    return `
        <div class="col">
            <div class="card shadow-lg my-4">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="cardsubtitle text-muted">${location}</h2>
                <p class="card-text">${description}</p>
                </div>
                <div class="card-body border-top p-3 bg-light text-dark">
                <p class="date-info mb-0"> ${startdateformatted} - ${enddateformatted}</p>
                </div>
            </div>
        </div>
    `;
}


function alert(message) {
    var alertthing = document.querySelector(`main`)
    var wrapper = document.createElement('div')
    wrapper.innerHTML = '<div class="alert alert-primary" role="alert">' + message + '</div>'

    alertthing.append(wrapper)
}


window.addEventListener('DOMContentLoaded', async () => {



    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);
      if (!response.ok) {
        const fetchalert = "There was a problem fetching the conference list";
        alert(fetchalert);
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const name = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const startdate = new Date(details.conference.starts);
                const startdateformatted = startdate.toLocaleDateString();
                const enddate = new Date(details.conference.ends);
                const enddateformatted = enddate.toLocaleDateString();
                const location = details.conference.location.name;
                const html = createCard(name, description, pictureUrl, startdateformatted, enddateformatted, location);
                const column = document.querySelector(`.row`);
                column.innerHTML += html;
            }
        }

      }
    } catch (e) {
        console.error(e);
        const displayalert = "There was a problem processing the conference data";
        alert(displayalert);
      // Figure out what to do if an error is raised
    }

  });
