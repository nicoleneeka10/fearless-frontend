window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/'

    try {
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json()


        const selectTag = document.getElementById('state');
        for (let state of data.states) {
            const name = state.name;
            const abbreviation = state.abbreviation;
            const html = `<option value=${abbreviation}>${name}</option>`;
            const optionTag = document.getElementById('state');
            optionTag.innerHTML += html;

        }
    }
    } catch (e) {
        console.error(e);
    }


    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));




    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchCongif = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
        },
    }
    const response = await fetch(locationUrl, fetchCongif);
    if (response.ok) {
        formTag.reset();
        const newLocation = await response.json();
    }
    });
});
